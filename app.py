from flask import Flask, render_template,redirect, url_for, request

app = Flask(__name__)

@app.route('/')
def home():
	return "Enter /login in the url"

@app.route('/welcome')
def welcome():
	return "WELCOME! LOGIN SUCCESSFUL!"

@app.route('/login',methods=['GET','POST'])
def login():
	error=None
	if request.method=="POST":
		if request.form['username']!= 'admin' or request.form['password']!= 'admin':
			error = 'Invalid Username or Password, try again!'
		else:
			return redirect(url_for('welcome'))
	return render_template('test.html',error=error)


if __name__ =='__main__':
	app.run(debug=True)